# "Fun with ZFS" tech talk

Presented [here](https://www.youtube.com/watch?v=kW6oN2p6sWU).

## Showing the presentation

`presentation.slide` can be shown using
[Golang present](https://godoc.org/golang.org/x/tools/cmd/present).

```bash
$ go get golang.org/x/tools/cmd/present
$ present -notes
2018/10/23 15:33:55 Open your web browser and visit http://127.0.0.1:3999
2018/10/23 15:33:55 Notes are enabled, press 'N' from the browser to display them.
```
